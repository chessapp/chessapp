package hu.kuiters.dao;

import hu.kuiters.chessapp.dao.UserDAO;
import hu.kuiters.chessapp.dto.UserDTO;
import hu.kuiters.chessapp.services.UserService;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

/**
 *
 * @author Zsolt
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    UserDAO userDAO;
    
    UserService us;
    
    public UserServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        List<UserDTO> userDTO = new ArrayList<>();
        UserDTO jozsi = new UserDTO();
        jozsi.setEmail("valami@valami.hu");
        jozsi.setNickName("Joco");
        jozsi.setPassword("valamike");
        UserDTO feco = new UserDTO();
        feco.setEmail("feco@valami.hu");
        feco.setNickName("Fecoka");
        feco.setPassword("feco");
        UserDTO sandor = new UserDTO();
        sandor.setEmail("sanya@valami.hu");
        sandor.setNickName("sanya");
        sandor.setPassword("sanya");

        userDTO.add(jozsi);
        userDTO.add(feco);
        userDTO.add(sandor);

        Mockito.when(userDAO.getUsers()).thenReturn(userDTO);
        us = new UserService(userDAO);

  
    }

    @After
    public void tearDown() {

    }

    @Test
    public void getUsers() {
        List<UserDTO> users = us.getUsers();

        assertEquals(3, users.size());
    }
        
    @Test
    public void testGetPlayer(){

    }
}
