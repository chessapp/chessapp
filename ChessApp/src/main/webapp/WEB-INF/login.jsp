<%-- 
    Document   : login
    Created on : Aug 27, 2020, 2:00:54 PM
    Author     : Zsolt
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bejelentkezés</title>
        <%@include file="menu.jsp" %>
    </head>
    <style>
        body{
            background-color:#e6e6e6;
            display: flex;
            flex-direction: column;

        }
    </style>
    <body>
        <div class="container">

            <form action="loginServlet" method="post">
                <label for="email">Email</label>
                <input type="text" palceholder="Enter Email" name="email" required="true">

                <label for="password">Password</label>
                <input type="password" palceholder="Enter Password" name="password" required="true">

                <button type="submit">Belépés</button>
            </form>
        </div>
    </body>
    <%@include file="footer.jsp" %>
</html>
