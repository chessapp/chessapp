<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Folyamatban lévő játékok</title>


    </head>
    <%@include file="menu.jsp" %>
    <style>
        body{
            background-color:#e6e6e6;
            display: flex;
            flex-direction: column;

        }
    </style>
    <body>
        <div class="container">


            <div class="row">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Világos játékos</th>
                            <th>Sötét játékos</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${gameList}" var="game">
                            <tr>
                                <td>${game.id}</td>
                                <td>${game.playerLight.nickName}</td>
                                <td>${game.playerDark.nickName}</td>
                                <td><a class="button" href="chooseGameInProgressServlet?gameId=${game.id}">játék betöltése</a></td>
                            </tr>
                        </c:forEach>


                    </tbody>

                </table>
            </div>
    </body>
    <%@include file="footer.jsp" %>
</html>
