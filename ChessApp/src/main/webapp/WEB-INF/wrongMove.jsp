<%-- 
    Document   : wrongMove
    Created on : Sep 5, 2020, 9:06:51 PM
    Author     : scher
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file="menu.jsp" %>
        <title>Rossz lépés</title>
    </head>
    <style>
        body{
            background-color:#e6e6e6;
            display: flex;
            flex-direction: column;

        }
    </style>
    <body>
        <h1>Rossz lépés, katt az alábbi linkre válaszd ki újra a játékot és probálj jót lépni</h1>
        <a href="${pageContext.request.contextPath}/listGameInProgressServlet">ide kattintva megkapod a folyamatban lévő játékok listáját</a>
    </body>
    <%@include file="footer.jsp" %>
</html>
