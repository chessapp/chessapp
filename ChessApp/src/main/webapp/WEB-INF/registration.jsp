<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Regisztráció</title>


        <%@include file="menu.jsp" %>
    </head>
    <style>
        body{
            background-color:#e6e6e6;
            display: flex;
            flex-direction: column;

        }
    </style>
    <body>
        <div class="container">

            <div class="alert alert-danger" id="errorBox" style="display: none;">
                <strong>Nem valid a form!</strong>
            </div>
            <form method="POST" action="registrationServlet">
                <div class="form-group">
                    <label for="email">Email address:</label>
                    <input type="email" class="form-control" placeholder="Enter email" id="email" name="email">
                </div>
                <div class="form-group">
                    <label for="nickName">Nickname:</label>
                    <input type="text" class="form-control" placeholder="Enter nick name" name="nickName" id="nickName">
                </div>
                <div class="form-group">
                    <label for="password">password:</label>
                    <input type="text" class="form-control" placeholder="Enter password" name="password" id="password">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </body>
    <%@include file="footer.jsp" %>
</html>
