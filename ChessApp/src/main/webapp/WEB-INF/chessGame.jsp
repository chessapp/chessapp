<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@include file="menu.jsp" %>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8"> 
        <title>Sakktábla</title>
        <style type="text/css">
            body{
                background-color:#e6e6e6;
                display: flex;
                flex-direction: column;
            }

            .page {
            }

            .chessboard {
                float: left;
                width: 475px;
                height: 475px;
                margin: 20px;
                border: 25px solid #333;
            }
            .black {
                float: left;
                width: 53px;
                height: 53px;
                background-color: #999;
                font-size:50px;
                text-align:center;
                display: table-cell;
                vertical-align:middle;
            }
            .white {
                float: left;
                width: 53px;
                height: 53px;
                background-color: #fff;
                font-size:50px;
                text-align:center;
                display: table-cell;
                vertical-align:middle;
            }

            .handlingField {
                float: left;

            }

            .movmentHandlingField {
                float: inside;

            }

            .pageNavigatorField {
                float: top;

            }

            .stepCountField {
                float: bottom;

            }

        </style>

    </head>
    <body>
        <div class="page">

            <div class="chessboard">
                <!-- 1st -->
                <c:forEach items="${chessTableArray}" var="row" varStatus="rowIndex">
                    <c:forEach items="${row}" var="cell" varStatus="colIndex">

                        <c:choose>

                            <c:when test="${rowIndex.index % 2 == 0 && colIndex.index % 2 == 0}">

                                <div class="white">${cell}</div>

                            </c:when>

                            <c:when test="${rowIndex.index % 2 != 0 && colIndex.index % 2 != 0}">

                                <div class="white">${cell}</div>

                            </c:when>

                            <c:otherwise>
                                <div class="black">${cell}</div>
                            </c:otherwise>

                        </c:choose>
                    </c:forEach>
                </c:forEach>
            </div class="chessboard">

            <br>
            <div class="handlingFiled">
                <div class="pageNavigatorField">
                    <a href="${pageContext.request.contextPath}/listGameInProgressServlet" class="btn btn-info">Folyamatban lévő játékok</a>

                </div>
                <br>
                <div class="movmentHandlingField">
                    <form method="POST" action="chessGameServlet">
                        <label>Honnan:</label>

                        <select name="actualPosX" id="actualPosX">
                            <option value=""></option>
                            <option value="a">a</option>
                            <option value="b">b</option>
                            <option value="c">c</option>
                            <option value="d">d</option>
                            <option value="e">e</option>
                            <option value="f">f</option>
                            <option value="g">g</option>
                            <option value="h">h</option>
                        </select>

                        <select name="actualPosY" id="actualPosY">
                            <option value=""></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                        </select>


                        <label>Hova:</label>

                        <select name="targetPosX" id="targetPosX">
                            <option value=""></option>
                            <option value="a">a</option>
                            <option value="b">b</option>
                            <option value="c">c</option>
                            <option value="d">d</option>
                            <option value="e">e</option>
                            <option value="f">f</option>
                            <option value="g">g</option>
                            <option value="h">h</option>
                        </select>

                        <select name="targetPosY" id="targetPosY">
                            <option value=""></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                        </select>


                        <input type="submit" <c:if test="${isActive == false}"><c:out value="disabled='disabled'"/></c:if>">
                        </form>
                    </div class="movmentHandlingField">



                </div class="handlingFiled">

                <div class="stepCountFiled">

                <c:set var = "stepCount" scope = "session" value = "${stepCount}"/>

                <p>Lépések száma: ${stepCount}</p>

                <c:choose>
                    <c:when test="${stepCount % 2 == 0}">

                        <p>Világos játékos lép</p>

                    </c:when>

                    <c:when test="${stepCount % 2 != 0}">

                        <p>Sötét játékos lép</p>

                    </c:when>

                    <c:otherwise>
                        <p></p>
                    </c:otherwise>
                </c:choose>

            </div>
        </div class="stepCountFiled">

    </div class="pageNavigatorField">

</body>

</html>
<%@include file="footer.jsp" %>