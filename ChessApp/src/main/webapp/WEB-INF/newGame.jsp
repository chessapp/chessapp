<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <%@include file="menu.jsp" %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Új játék</title>
    </head>
    <style>
        body{
            background-color:#e6e6e6;
            display: flex;
            flex-direction: column;

        }
    </style>
    <body>

        <div class="container">


            <div class="alert alert-danger" id="errorBox" style="display: none;">
                <strong>Nem valid a form!</strong>
            </div>


            <a href="${pageContext.request.contextPath}/listUserServlet" class="btn btn-info">Játékosok listája</a>
            <br/>
            <form method="POST" action="newGameServlet">
                <div class="form-group">
                    <label for="playerLight">Világos játékos:</label>
                    <input type="text" class="form-control" placeholder="Világos játékos neve" id="playerLight" name="playerLight">
                </div>
                <div class="form-group">
                    <label for="playerDark">Sötét játékos:</label>
                    <input type="text" class="form-control" placeholder="Sötét játékos neve" name="playerDark" id="playerDark">
                </div>

                <button type="submit" class="btn btn-primary">Játék Indítása</button>
            </form>
        </div>
    </body>
    <%@include file="footer.jsp" %>
</html>
