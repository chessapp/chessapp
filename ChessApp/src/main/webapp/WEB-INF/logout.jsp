<%-- 
    Document   : logout
    Created on : Sep 10, 2020, 2:32:47 PM
    Author     : Zsolt
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <%@include file="menu.jsp" %>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">  
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <title>Logout Page</title>

    </head>
    <style>
        body{
            background-color:#e6e6e6;
            display: flex;
            flex-direction: column;

        }
    </style>
    <body>


        <br/>
        <div class="alert alert-success">
            <strong>Sikeres kijelentkezés!</strong> Köszi,hogy benéztél!
        </div>

    </body>
    <%@include file="footer.jsp" %>
</html>
