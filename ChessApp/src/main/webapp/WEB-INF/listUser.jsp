<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Játékosok</title>
        <%@include file="menu.jsp" %>
    </head>
    <style>
        body{
            background-color:#e6e6e6;
            display: flex;
            flex-direction: column;

        }
    </style>
    <body>
        <div class="container">


            <div class="row">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>Email</th>
                            <th>NickName</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${userList}" var="user">
                            <tr>
                                <td>${user.id}</td>
                                <td>${user.email}</td>
                                <td>${user.nickName}</td>

                            </tr>
                        </c:forEach>


                    </tbody>

                </table>
                <a href="${pageContext.request.contextPath}/newGameServlet" class="btn btn-info">Vissza a játék kezdésére</a>
            </div>
        </div>
    </body>
    <%@include file="footer.jsp" %>
</html>
