<%-- 
    Document   : errorPage
    Created on : Aug 27, 2020, 7:33:17 PM
    Author     : Zsolt
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <style>
            body{
            background-color:#e6e6e6;
            display: flex;
            flex-direction: column;

        }
        </style>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error</title>
    </head>
    <body>
        <h1>Hibás bejelentkezés!</h1>
        <a href="loginServlet">Vissza a bejelentkezéshez</a>
    </body>
    <%@include file="footer.jsp" %>
</html>
