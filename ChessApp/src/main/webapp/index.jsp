
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Főoldal</title>
        <title>Levelező sakk</title>
        <%@include file="WEB-INF/menu.jsp" %>
    </head>
    <style>
        pre{
            background: #e6e6e6;
            font-size: 22px;
            margin: 1em 0;
            display: block;
            white-space: pre-wrap;
            white-space: -moz-pre-wrap;
            white-space: -o-pre-wrap;
            word-wrap: break-word;
            word-break: keep-all
        }
        body{
            background-color:#e6e6e6;
            display: flex;
            flex-direction: column;

        }
    </style>
    <body>

        <h1>Levelezős sakkjáték</h1>
        <h2>A játék menete:</h2>
        <pre>
Ha még nem regisztráltál akkor regisztrálj, utána új játék kezdése oldalon válaszd ki az ellenfelet (csak regisztráltak közül lehet választani).
Amennyiben léptél egy bábuval, a partnered kap egy emailt, amiben le van írva,hogy melyik pozícióról hova léptél.
A játék annyira figyeli a helyes lépéseket mint egy mágneses sakk készlet ergo semennyire, ügyesen megtartja a bábukat és kész.
A játék figyelembe veszi, ha saját bábut szeretnél mozgatni, illetve saját bábut szeretnél leütni. A partnered bábuival nem tudsz lépni.

A szabályos lépéseket a program nem kezeli, úgy hogy, kérlek tanulmányozd a szabályokat.

<a href="https://hu.wikipedia.org/wiki/Sakk" class="btn btn-info">Sakk játékszabályok</a>

Lépésekben gazdag játékot (kivéve ha gyorsan akarsz nyerni).
        </pre>
        <%@include file="WEB-INF/footer.jsp" %>
    </body>
</html>
