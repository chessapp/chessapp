package hu.kuiters.chessapp.dto;

import lombok.Data;
import lombok.ToString;


@Data
@ToString
public class GamePieceDTO extends BaseDTO {

    private char posX;
    private int posY;
    private boolean isActive;

    private PieceDTO piece;
    private GameDTO game;

}
