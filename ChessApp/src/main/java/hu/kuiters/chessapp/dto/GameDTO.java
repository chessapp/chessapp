package hu.kuiters.chessapp.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class GameDTO extends BaseDTO {
    
    private int stepCount;

    private UserDTO playerLight;

    private UserDTO playerDark;

    private List<GamePieceDTO> gamePieceArray;
}
