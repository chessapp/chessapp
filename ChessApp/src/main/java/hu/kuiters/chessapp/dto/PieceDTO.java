package hu.kuiters.chessapp.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author User
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PieceDTO extends BaseDTO {

    private String color;
    private String name;
    private String unicode;
}
