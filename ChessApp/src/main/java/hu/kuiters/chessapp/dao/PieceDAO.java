package hu.kuiters.chessapp.dao;

import hu.kuiters.chessapp.entity.PieceEntity;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Singleton
public class PieceDAO {

    private static final String QUERY_FIND_PIECE = "SELECT p from PieceEntity p where name = :name";

    @PersistenceContext
    private EntityManager em;

    public PieceEntity getPiece(String name) {
        PieceEntity piece;
        Query pieceQuery = em.createQuery(QUERY_FIND_PIECE, PieceEntity.class);
        pieceQuery.setParameter("name", name);
        return piece = (PieceEntity) pieceQuery.getSingleResult();
    }

}
