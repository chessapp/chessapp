package hu.kuiters.chessapp.dao;

import hu.kuiters.chessapp.dto.UserDTO;
import hu.kuiters.chessapp.entity.UserEntity;
import hu.kuiters.chessapp.mappers.UserMapper;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Zsolt
 */
@Singleton
@TransactionAttribute
public class LoginDAO {

    private static final String QUERY_LOGIN = "SELECT u FROM UserEntity u WHERE email=:email and password=:password";

    @PersistenceContext
    private EntityManager em;

    public UserDTO login(String email, String password) {

        Query query = em.createQuery(QUERY_LOGIN, UserEntity.class);
        query.setParameter("email", email);
        query.setParameter("password", password);

        UserEntity userEntity;
        try {
            userEntity = (UserEntity) query.getSingleResult();
            return UserMapper.INSTANCE.toDTO(userEntity);
        } catch (NoResultException e) {
            System.out.println("A felhasználó nem létezik");
            userEntity = null;
            return UserMapper.INSTANCE.toDTO(userEntity);
        }
//        UserEntity userEntity = (UserEntity) query.getSingleResult();
//        if (userEntity == null) {
//            System.out.println("A felhasználó nem létezik");
//            return null;
//        } else if (query.getResultList().contains(":email")) {
//            System.out.println("Nem megfelelő jelszó.");
//            userDTO.setValidLogin(false);
//        } else if (query.getResultList().contains(":password")) {
//            System.out.println("Nem megfelelő email cím!");
//
//        } else {
//            
//            return UserMapper.INSTANCE.toDTO(userEntity);
//
//        }

    }
}

/*
query = 'select u from user where u.email = :email;
setparameter(email, "tamas")
select u from user where u.email = 'tamas'
 */
