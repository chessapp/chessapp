package hu.kuiters.chessapp.dao;

import hu.kuiters.chessapp.dto.GameDTO;
import hu.kuiters.chessapp.dto.GamePieceDTO;
import hu.kuiters.chessapp.entity.GameEntity;
import hu.kuiters.chessapp.entity.GamePiecesEntity;
import hu.kuiters.chessapp.entity.UserEntity;
import hu.kuiters.chessapp.mappers.GameMapper;
import hu.kuiters.chessapp.mappers.GamePieceMapper;
import hu.kuiters.chessapp.util.ArrayListTo2DArrayConverter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
public class GameDAO {

    private static final String QUERY_ALL_GAME = "select g from GameEntity g";

    @Inject
    StartingPositionDAO startingPositionDAO;

    @Inject
    GamePieceDAO gamePieceDAO;

    @PersistenceContext
    private EntityManager em;

    public List<GameDTO> getAllGame() {
        List<GameEntity> players = em.createQuery(QUERY_ALL_GAME).getResultList();
        List<GameDTO> gameDTOs = new ArrayList();
        for (GameEntity player : players) {
            gameDTOs.add(GameMapper.INSTANCE.toDTO(player));
        }

        return gameDTOs;
    }

    public GameDTO getGameById(long gameId) {
        GameEntity gameEntity = em.find(GameEntity.class, gameId);
        GameDTO gameDTO = GameMapper.INSTANCE.toDTO(gameEntity);

        List<GamePiecesEntity> gamePieceEntities = gamePieceDAO.getActiveGamePieces(gameEntity);
        List<GamePieceDTO> gamePieceDTOs = GamePieceMapper.INSTANCE.toDTOList(gamePieceEntities);

        gameDTO.setGamePieceArray(gamePieceDTOs);

        return gameDTO;
    }

    public void increaseStepCount(long gameId) {
        GameEntity gameEntity = em.find(GameEntity.class, gameId);
        int actualStepCount = gameEntity.getStepCount();
        gameEntity.setStepCount(++actualStepCount);
        
        em.merge(gameEntity);

    }

    public void startNewGame(UserEntity lightPlayer, UserEntity darkPlayer) {
        GameEntity newGameEntity = new GameEntity();
        newGameEntity.setPlayerLight(lightPlayer);
        newGameEntity.setPlayerDark(darkPlayer);
        newGameEntity.setStepCount(0);

        em.persist(newGameEntity);

        startingPositionDAO.initBoard(newGameEntity);
    }

}
