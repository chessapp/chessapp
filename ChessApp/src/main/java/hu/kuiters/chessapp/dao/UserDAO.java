package hu.kuiters.chessapp.dao;

import hu.kuiters.chessapp.dto.GameDTO;
import hu.kuiters.chessapp.dto.UserDTO;
import hu.kuiters.chessapp.entity.UserEntity;
import hu.kuiters.chessapp.mappers.UserMapper;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 *
 * @author Zsolt
 */
@Singleton
@NoArgsConstructor
@AllArgsConstructor
public class UserDAO {

    private static final String QUERY_FIND_ALL_USERS = "SELECT u FROM UserEntity u";
    private static final String QUERY_USER_FOR_NEW_GAME = "select u from UserEntity u where nick_name = :name";

    @PersistenceContext
    private EntityManager em;
    
    @Inject
    GameDAO gameDAO;

    public List<UserDTO> getUsers() {
        List<UserEntity> users = em.createQuery(QUERY_FIND_ALL_USERS).getResultList();
        List<UserDTO> userDTOs = new ArrayList();
        for (UserEntity user : users) {
            userDTOs.add(UserMapper.INSTANCE.toDTO(user));
        }

        return userDTOs;
    }

    public void addNewUser(String email, String nickName, String password) {
        UserEntity ue = new UserEntity();
        ue.setEmail(email);
        ue.setNickName(nickName);
        ue.setPassword(password);
        em.persist(ue);
    }

    public UserEntity getPlayer(String nickName) {
        UserEntity player;
        Query ligthQuery = em.createQuery(QUERY_USER_FOR_NEW_GAME, UserEntity.class);
        ligthQuery.setParameter("name", nickName);

        return player = (UserEntity) ligthQuery.getSingleResult();
    }
    
      
    public UserDTO getPlayerByStepCount(long gameId) {
        
        GameDTO gameDTO = gameDAO.getGameById(gameId);
        UserDTO playerDTO;
                
        if (gameDTO.getStepCount() %2 == 0) {
            
            playerDTO = gameDTO.getPlayerLight();
            
            } else {
            
            playerDTO = gameDTO.getPlayerDark();
        }
        
        return playerDTO;
        
    }
}
