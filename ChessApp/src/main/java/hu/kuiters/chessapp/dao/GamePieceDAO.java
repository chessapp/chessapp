package hu.kuiters.chessapp.dao;

import hu.kuiters.chessapp.dto.GamePieceDTO;
import hu.kuiters.chessapp.entity.GameEntity;
import hu.kuiters.chessapp.entity.GamePiecesEntity;
import hu.kuiters.chessapp.exception.WrongSelectedPiece;
import hu.kuiters.chessapp.mappers.GamePieceMapper;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Singleton
@TransactionAttribute
public class GamePieceDAO {

    private static final String QUERY_FIND_ALL_GAME_PIECES = "SELECT g FROM GamePiecesEntity g WHERE game_id = :gameid AND isActive = :isActive";
    private static final String QUERY_FIND_SELECTED_PIECE = "SELECT g FROM GamePiecesEntity g WHERE game_id = :gameid AND posX = :posX AND posY = :posY";

    @PersistenceContext
    private EntityManager em;

    public List<GamePiecesEntity> getActiveGamePieces(GameEntity gameEntity) {
        Query gamePieceQuery = em.createQuery(QUERY_FIND_ALL_GAME_PIECES, GamePiecesEntity.class);
        gamePieceQuery.setParameter("gameid", gameEntity.getId());
        gamePieceQuery.setParameter("isActive", true);
        List<GamePiecesEntity> gamePieceEntity = gamePieceQuery.getResultList();

        return gamePieceEntity;
    }

    public List<GamePieceDTO> getSelectedGamePiece(Long gameid, char actualPosX, int actualPosY) {

        Query gamePieceQuery = em.createQuery(QUERY_FIND_SELECTED_PIECE, GamePiecesEntity.class);
        gamePieceQuery.setParameter("gameid", gameid);
        gamePieceQuery.setParameter("posX", actualPosX);
        gamePieceQuery.setParameter("posY", actualPosY);

        List<GamePiecesEntity> gamePieceEntity = gamePieceQuery.getResultList();
        List<GamePieceDTO> gamePieceDTOs = GamePieceMapper.INSTANCE.toDTOList(gamePieceEntity);
        return gamePieceDTOs;
    }

    public void setTargetPositionForGamePiece(GamePieceDTO gamePieceDTO, char targetPosX, int targetPosY) {

        GamePiecesEntity gamePieceEntity = GamePieceMapper.INSTANCE.toEntity(gamePieceDTO);

        gamePieceEntity.setPosX(targetPosX);
        gamePieceEntity.setPosY(targetPosY);
        em.merge(gamePieceEntity);

    }

    public void deleteGamePieceKnockdOut(GamePieceDTO gamePieceDTO) {

        GamePiecesEntity gamePieceEntity = GamePieceMapper.INSTANCE.toEntity(gamePieceDTO);
        gamePieceEntity.setActive(false);

        em.merge(gamePieceEntity);
    }

}
