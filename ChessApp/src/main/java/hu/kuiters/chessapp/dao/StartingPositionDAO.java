package hu.kuiters.chessapp.dao;

import hu.kuiters.chessapp.entity.GameEntity;
import hu.kuiters.chessapp.entity.GamePiecesEntity;
import hu.kuiters.chessapp.entity.PieceEntity;
import javax.ejb.Stateful;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateful
public class StartingPositionDAO {

    @PersistenceContext
    private EntityManager em;

    @Inject
    private PieceDAO pieceDAO;

    public void initBoard(GameEntity gameEntity) {

//black king
        PieceEntity blackKing = pieceDAO.getPiece("black_king");

        GamePiecesEntity blackKingEntity = new GamePiecesEntity();
        blackKingEntity.setPosX('e');
        blackKingEntity.setPosY(8);
        blackKingEntity.setPiece(blackKing);
        blackKingEntity.setGame(gameEntity);
        blackKingEntity.setActive(true);

        em.persist(blackKingEntity);

//white king
        PieceEntity whiteKing = pieceDAO.getPiece("white_king");

        GamePiecesEntity whiteKingEntity = new GamePiecesEntity();
        whiteKingEntity.setPosX('e');
        whiteKingEntity.setPosY(1);
        whiteKingEntity.setPiece(whiteKing);
        whiteKingEntity.setGame(gameEntity);
        whiteKingEntity.setActive(true);

        em.persist(whiteKingEntity);

//black queen
        PieceEntity blackQueen = pieceDAO.getPiece("black_queen");

        GamePiecesEntity blackQueenEntity = new GamePiecesEntity();
        blackQueenEntity.setPosX('d');
        blackQueenEntity.setPosY(8);
        blackQueenEntity.setPiece(blackQueen);
        blackQueenEntity.setGame(gameEntity);
        blackQueenEntity.setActive(true);

        em.persist(blackQueenEntity);

//white queen
        PieceEntity whiteQueen = pieceDAO.getPiece("white_queen");

        GamePiecesEntity whiteQueenEntity = new GamePiecesEntity();
        whiteQueenEntity.setPosX('d');
        whiteQueenEntity.setPosY(1);
        whiteQueenEntity.setPiece(whiteQueen);
        whiteQueenEntity.setGame(gameEntity);
        whiteQueenEntity.setActive(true);

        em.persist(whiteQueenEntity);

//2 black bishop
        PieceEntity blackBishop = pieceDAO.getPiece("black_bishop");

        GamePiecesEntity blackBishopEntity1 = new GamePiecesEntity();
        blackBishopEntity1.setPosX('c');
        blackBishopEntity1.setPosY(8);
        blackBishopEntity1.setPiece(blackBishop);
        blackBishopEntity1.setGame(gameEntity);
        blackBishopEntity1.setActive(true);

        em.persist(blackBishopEntity1);

        GamePiecesEntity blackBishopEntity2 = new GamePiecesEntity();
        blackBishopEntity2.setPosX('f');
        blackBishopEntity2.setPosY(8);
        blackBishopEntity2.setPiece(blackBishop);
        blackBishopEntity2.setGame(gameEntity);
        blackBishopEntity2.setActive(true);

        em.persist(blackBishopEntity2);

//2 white bishop
        PieceEntity whiteBishop = pieceDAO.getPiece("white_bishop");

        GamePiecesEntity whiteBishopEntity1 = new GamePiecesEntity();
        whiteBishopEntity1.setPosX('c');
        whiteBishopEntity1.setPosY(1);
        whiteBishopEntity1.setPiece(whiteBishop);
        whiteBishopEntity1.setGame(gameEntity);
        whiteBishopEntity1.setActive(true);

        em.persist(whiteBishopEntity1);

        GamePiecesEntity whiteBishopEntity2 = new GamePiecesEntity();
        whiteBishopEntity2.setPosX('f');
        whiteBishopEntity2.setPosY(1);
        whiteBishopEntity2.setPiece(whiteBishop);
        whiteBishopEntity2.setGame(gameEntity);
        whiteBishopEntity2.setActive(true);

        em.persist(whiteBishopEntity2);

//2 black knight
        PieceEntity blackKnight = pieceDAO.getPiece("black_knight");

        GamePiecesEntity blackKnightEntity1 = new GamePiecesEntity();
        blackKnightEntity1.setPosX('b');
        blackKnightEntity1.setPosY(8);
        blackKnightEntity1.setPiece(blackKnight);
        blackKnightEntity1.setGame(gameEntity);
        blackKnightEntity1.setActive(true);

        em.persist(blackKnightEntity1);

        GamePiecesEntity blackKnightEntity2 = new GamePiecesEntity();
        blackKnightEntity2.setPosX('g');
        blackKnightEntity2.setPosY(8);
        blackKnightEntity2.setPiece(blackKnight);
        blackKnightEntity2.setGame(gameEntity);
        blackKnightEntity2.setActive(true);

        em.persist(blackKnightEntity2);

//2 white knight
        PieceEntity whiteKnight = pieceDAO.getPiece("white_knight");

        GamePiecesEntity whiteKnightEntity1 = new GamePiecesEntity();
        whiteKnightEntity1.setPosX('b');
        whiteKnightEntity1.setPosY(1);
        whiteKnightEntity1.setPiece(whiteKnight);
        whiteKnightEntity1.setGame(gameEntity);
        whiteKnightEntity1.setActive(true);

        em.persist(whiteKnightEntity1);

        GamePiecesEntity whiteKnightEntity2 = new GamePiecesEntity();
        whiteKnightEntity2.setPosX('g');
        whiteKnightEntity2.setPosY(1);
        whiteKnightEntity2.setPiece(whiteKnight);
        whiteKnightEntity2.setGame(gameEntity);
        whiteKnightEntity2.setActive(true);

        em.persist(whiteKnightEntity2);

//2 black rook
        PieceEntity blackRook = pieceDAO.getPiece("black_rook");

        GamePiecesEntity blackRookEntity1 = new GamePiecesEntity();
        blackRookEntity1.setPosX('a');
        blackRookEntity1.setPosY(8);
        blackRookEntity1.setPiece(blackRook);
        blackRookEntity1.setGame(gameEntity);
        blackRookEntity1.setActive(true);

        em.persist(blackRookEntity1);

        GamePiecesEntity blackRookEntity2 = new GamePiecesEntity();
        blackRookEntity2.setPosX('h');
        blackRookEntity2.setPosY(8);
        blackRookEntity2.setPiece(blackRook);
        blackRookEntity2.setGame(gameEntity);
        blackRookEntity2.setActive(true);

        em.persist(blackRookEntity2);

//2 white rook
        PieceEntity whiteRook = pieceDAO.getPiece("white_rook");

        GamePiecesEntity whiteRookEntity1 = new GamePiecesEntity();
        whiteRookEntity1.setPosX('a');
        whiteRookEntity1.setPosY(1);
        whiteRookEntity1.setPiece(whiteRook);
        whiteRookEntity1.setGame(gameEntity);
        whiteRookEntity1.setActive(true);

        em.persist(whiteRookEntity1);

        GamePiecesEntity whiteRookEntity2 = new GamePiecesEntity();
        whiteRookEntity2.setPosX('h');
        whiteRookEntity2.setPosY(1);
        whiteRookEntity2.setPiece(whiteRook);
        whiteRookEntity2.setGame(gameEntity);
        whiteRookEntity2.setActive(true);

        em.persist(whiteRookEntity2);

//8 black pawn
        for (int i = 1; i <= 8; i++) {
            PieceEntity blackPawn = pieceDAO.getPiece("black_pawn");
            char posX = (char) (i + 96); //in ascii a = 97, b = 98....
            int posY = 7;

            GamePiecesEntity gamePiece = new GamePiecesEntity();
            gamePiece.setPosX(posX);
            gamePiece.setPosY(posY);
            gamePiece.setPiece(blackPawn);  //itt majd figyelni hogy pontosan mire neveztük el ezt pontosan
            gamePiece.setGame(gameEntity);
            gamePiece.setActive(true);

            em.persist(gamePiece);

        }

//8 white pawn
        for (int i = 1; i <= 8; i++) {
            PieceEntity whitePawn = pieceDAO.getPiece("white_pawn");
            char posX = (char) (i + 96); //in ascii a = 97, b = 98....
            int posY = 2;

            GamePiecesEntity gamePiece = new GamePiecesEntity();
            gamePiece.setPosX(posX);
            gamePiece.setPosY(posY);
            gamePiece.setPiece(whitePawn);  //itt majd figyelni hogy pontosan mire neveztük el ezt pontosan
            gamePiece.setGame(gameEntity);
            gamePiece.setActive(true);

            em.persist(gamePiece);
        }

    }

}
