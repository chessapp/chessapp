package hu.kuiters.chessapp.services;

import hu.kuiters.chessapp.dao.UserDAO;
import hu.kuiters.chessapp.dto.UserDTO;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 *
 * @author Zsolt
 */
@Singleton
@TransactionAttribute
@AllArgsConstructor
@NoArgsConstructor
public class UserService {

    @Inject
    private UserDAO userDAO;

    public List<UserDTO> getUsers() {
        return userDAO.getUsers();
    }

    public void addNewUser(String email, String nickName, String password) {
        userDAO.addNewUser(email, nickName, password);
    }

}
