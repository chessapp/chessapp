package hu.kuiters.chessapp.services;

import hu.kuiters.chessapp.dao.LoginDAO;
import hu.kuiters.chessapp.dto.UserDTO;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;

/**
 *
 * @author Zsolt
 */
@Singleton
@TransactionAttribute
public class LoginService {

    @Inject
    private LoginDAO loginDAO;

    public UserDTO login(String email, String password) {

        UserDTO userDTO = loginDAO.login(email, password);
        return userDTO;
    }

}
