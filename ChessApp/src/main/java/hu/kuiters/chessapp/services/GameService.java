package hu.kuiters.chessapp.services;

import hu.kuiters.chessapp.dao.GameDAO;
import hu.kuiters.chessapp.dao.GamePieceDAO;
import hu.kuiters.chessapp.dao.UserDAO;
import hu.kuiters.chessapp.dto.GameDTO;
import hu.kuiters.chessapp.dto.GamePieceDTO;
import hu.kuiters.chessapp.dto.UserDTO;
import hu.kuiters.chessapp.entity.GamePiecesEntity;
import hu.kuiters.chessapp.entity.UserEntity;
import java.util.List;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;

@Singleton
@TransactionAttribute
public class GameService {

    @Inject
    private GameDAO gameDAO;

    @Inject
    GamePieceDAO gamePieceDAO;

    @Inject
    UserDAO userDAO;

    public List<GameDTO> getAllGame() {
        return gameDAO.getAllGame();
    }

    public GameDTO getGameById(long gameId) {
        return gameDAO.getGameById(gameId);
    }

    public void increaseStepCount(long gameId) {
        gameDAO.increaseStepCount(gameId);
    }

    public void startNewGame(String ligthNickName, String darkNickName) {
        UserEntity darkPlayer = userDAO.getPlayer(darkNickName);
        UserEntity ligthPlayer = userDAO.getPlayer(ligthNickName);

        gameDAO.startNewGame(ligthPlayer, darkPlayer);
    }

    public List<GamePieceDTO> getSelectedGamePiece(Long gameid, char actualPosX, int actualPosY) {
        return gamePieceDAO.getSelectedGamePiece(gameid, actualPosX, actualPosY);
    }

    public void setTargetPositionForGamePiece(GamePieceDTO gamePieceDTO, char targetPosX, int targetPosY) {
        gamePieceDAO.setTargetPositionForGamePiece(gamePieceDTO, targetPosX, targetPosY);
    }

    public void deleteGamePieceKnockdOut(GamePieceDTO gamePieceDTO) {
        gamePieceDAO.deleteGamePieceKnockdOut(gamePieceDTO);
    }

    public boolean isOwenGamePieceOnTargetPos(GamePieceDTO gamePieceOnActualPos, GamePieceDTO gamePieceOnTargetPos) {

        String colorOfgamePieceOnActualPos = gamePieceOnActualPos.getPiece().getColor();
        String colorOfPieceOnTargetPos = gamePieceOnTargetPos.getPiece().getColor();

        if (colorOfgamePieceOnActualPos.equals(colorOfPieceOnTargetPos)) {
            return true;
        }
        return false;
    }
    
    public UserDTO getPlayerByStepCount(long gameId) {
        return userDAO.getPlayerByStepCount(gameId);
    }

}
