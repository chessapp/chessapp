package hu.kuiters.chessapp.entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author User
 */
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "user")
public class UserEntity extends BaseEntity {

    @Column(name = "nick_name")
    private String nickName;

    @Column
    private String email;

    @Column(unique = true)
    private String password;

    @OneToMany(mappedBy = "playerDark")
    private List<GameEntity> gamesDark;

    @OneToMany(mappedBy = "playerLight")
    private List<GameEntity> gamesLight;

    

}
