package hu.kuiters.chessapp.entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "piece")
public class PieceEntity extends BaseEntity {

    @Column
    private String color;

    @Column
    private String name;

    @Column
    private String unicode;

    @OneToMany(mappedBy = "piece")
    private List<GamePiecesEntity> gamePieces;

    public PieceEntity(String color, String name, String unicode) {
        this.color = color;
        this.name = name;
        this.unicode = unicode;
    }

}
