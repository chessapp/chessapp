package hu.kuiters.chessapp.entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;


@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "game")
public class GameEntity extends BaseEntity {
    
    @Column
    private int stepCount;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "player_dark_id")
    private UserEntity playerDark;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "player_light_id")
    private UserEntity playerLight;

    @OneToMany(mappedBy = "game")
    private List<GamePiecesEntity> gamePieces;
}
