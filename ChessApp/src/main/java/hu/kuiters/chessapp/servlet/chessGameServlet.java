package hu.kuiters.chessapp.servlet;

import hu.kuiters.chessapp.dto.GameDTO;
import hu.kuiters.chessapp.dto.GamePieceDTO;
import hu.kuiters.chessapp.dto.UserDTO;
import hu.kuiters.chessapp.entity.GamePiecesEntity;
import hu.kuiters.chessapp.services.GameService;
import hu.kuiters.chessapp.util.ArrayListTo2DArrayConverter;
import hu.kuiters.chessapp.util.EmailSender;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import javax.inject.Inject;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "chessGameServlet", urlPatterns = {"/chessGameServlet"})
public class chessGameServlet extends HttpServlet {

    @Inject
    GameService gameService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       
        req.getRequestDispatcher("WEB-INF/chessGame.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        char actualPosX = req.getParameter("actualPosX").charAt(0);
        int actualPosY = Integer.parseInt(req.getParameter("actualPosY"));
        char targetPosX = req.getParameter("targetPosX").charAt(0);
        int targetPosY = Integer.parseInt(req.getParameter("targetPosY"));
        HttpSession httpSession = req.getSession();
        Long gameId = (Long) httpSession.getAttribute("gameId");

        List<GamePieceDTO> gamePieceOnActualPos = gameService.getSelectedGamePiece(gameId, actualPosX, actualPosY);
        List<GamePieceDTO> gamePieceOnTargetPos = gameService.getSelectedGamePiece(gameId, targetPosX, targetPosY);

        if (gamePieceOnActualPos.isEmpty()) {

            resp.sendRedirect(req.getContextPath() + "/WrongMoveServlet");

        } else if (!gamePieceOnTargetPos.isEmpty()) {

            boolean isOwnPieceOnTargetPos = gameService.isOwenGamePieceOnTargetPos(gamePieceOnActualPos.get(0), gamePieceOnTargetPos.get(0));

            if (isOwnPieceOnTargetPos) {

                resp.sendRedirect(req.getContextPath() + "/WrongMoveServlet");

            } else {

                GamePieceDTO movingGamePieceDTO = gamePieceOnActualPos.get(0);
                gameService.setTargetPositionForGamePiece(movingGamePieceDTO, targetPosX, targetPosY);

                gameService.increaseStepCount(gameId);

                GamePieceDTO knockedOutGamePieceDTO = gamePieceOnTargetPos.get(0);
                gameService.deleteGamePieceKnockdOut(knockedOutGamePieceDTO);

                resp.sendRedirect(req.getContextPath() + "/chessGameServlet");

            }

        } else {

            GamePieceDTO gamePieceOnActualPosDTO = gamePieceOnActualPos.get(0);
            gameService.setTargetPositionForGamePiece(gamePieceOnActualPosDTO, targetPosX, targetPosY);

            gameService.increaseStepCount(gameId);

            resp.sendRedirect(req.getContextPath() + "/chessGameServlet");
        }

        System.out.println("l�p�s: " + actualPosX + actualPosY + targetPosX + targetPosY);
        String emilSubject = "l�p�s: " + actualPosX + actualPosY + targetPosX + targetPosY;

        System.out.println("gameId: " + httpSession.getAttribute("gameId"));

        UserDTO emailRecepient = gameService.getPlayerByStepCount(gameId);
        String emailBody = EmailSender.createEmailBody(actualPosX, actualPosY, targetPosX, targetPosY);

        EmailSender.sendEmail(emailRecepient.getEmail(), emilSubject, emailBody);

    }

}
