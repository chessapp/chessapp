package hu.kuiters.chessapp.servlet;

import hu.kuiters.chessapp.services.UserService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "listUserServlet", urlPatterns = {"/listUserServlet"})
public class listUserServlet extends HttpServlet {

    @Inject
    private UserService userService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setAttribute("userList", userService.getUsers());
        request.getRequestDispatcher("WEB-INF/listUser.jsp").forward(request, response);
    }

}
