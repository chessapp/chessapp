/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.kuiters.chessapp.servlet;

import hu.kuiters.chessapp.dto.UserDTO;
import hu.kuiters.chessapp.services.LoginService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Zsolt
 */
@WebServlet(name = "loginServlet", urlPatterns = {"/loginServlet"})
public class loginServlet extends HttpServlet {

    @Inject
    private LoginService loginService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();

        UserDTO user = (UserDTO) session.getAttribute("userDTO");
        
        if (user == null) {
            request.getRequestDispatcher("WEB-INF/login.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("WEB-INF/menu.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String email = request.getParameter("email");
        String password = request.getParameter("password");

        UserDTO userDTO = loginService.login(email, password);

        if (userDTO == null) {
            request.getRequestDispatcher("WEB-INF/errorPage.jsp").forward(request, response);
        } else {
            HttpSession session = request.getSession();
            session.setAttribute("userDTO", userDTO);
            request.getRequestDispatcher("WEB-INF/newGame.jsp").forward(request, response);
        }
    }
}
