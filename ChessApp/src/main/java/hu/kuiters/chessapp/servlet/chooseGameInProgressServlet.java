package hu.kuiters.chessapp.servlet;

import hu.kuiters.chessapp.dto.GameDTO;
import hu.kuiters.chessapp.dto.UserDTO;
import hu.kuiters.chessapp.services.GameService;
import hu.kuiters.chessapp.util.ArrayListTo2DArrayConverter;
import java.io.IOException;
import java.io.PrintWriter;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "chooseGameInProgressServlet", urlPatterns = {"/chooseGameInProgressServlet"})
public class chooseGameInProgressServlet extends HttpServlet {

    @Inject
    GameService gameService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        long gameId = Long.parseLong(req.getParameter("gameId"));
        HttpSession session = req.getSession();
        session.setAttribute("gameId", gameId);
        GameDTO gameDTO = gameService.getGameById(gameId);
        int stepCount = gameDTO.getStepCount();
        req.setAttribute("stepCount", stepCount);

        UserDTO user = (UserDTO) session.getAttribute("userDTO");
        boolean isActive = false;
        req.setAttribute("isActive", isActive);
        if (stepCount % 2 == 0 && user.getId() == gameDTO.getPlayerLight().getId()) {
            isActive = true;

        }

        if (stepCount % 2 != 0 && user.getId() == gameDTO.getPlayerDark().getId()) {
            isActive = true;

        }
        req.setAttribute("isActive", isActive);
        String[][] chessTableArray = new String[8][8];

        chessTableArray = ArrayListTo2DArrayConverter.placeUnicodesOfChessFigureIn2DArray(gameDTO.getGamePieceArray());

        req.setAttribute("chessTableArray", chessTableArray);

        req.getRequestDispatcher("/chessGameServlet").forward(req, resp);

    }

}
