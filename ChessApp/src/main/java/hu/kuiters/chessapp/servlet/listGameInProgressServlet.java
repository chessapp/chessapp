package hu.kuiters.chessapp.servlet;

import hu.kuiters.chessapp.services.GameService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "listGameInProgressServlet", urlPatterns = {"/listGameInProgressServlet"})
public class listGameInProgressServlet extends HttpServlet {

    @Inject
    private GameService gameService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setAttribute("gameList", gameService.getAllGame());

        req.getRequestDispatcher("WEB-INF/listGameInProgress.jsp").forward(req, resp);
    }

}
