package hu.kuiters.chessapp.servlet;

import hu.kuiters.chessapp.dto.GameDTO;
import hu.kuiters.chessapp.dto.UserDTO;
import hu.kuiters.chessapp.services.GameService;
import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(name = "newGameServlet", urlPatterns = {"/newGameServlet"})
public class newGameServlet extends HttpServlet {

    @Inject
    private GameService gameService;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();

        UserDTO user = (UserDTO) session.getAttribute("userDTO");
        if (user == null) {
            req.getRequestDispatcher("WEB-INF/login.jsp").forward(req, resp);
        } else {
            req.getRequestDispatcher("WEB-INF/newGame.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String darkPlayerName = req.getParameter("playerDark");
        String lightPlayerName = req.getParameter("playerLight");
        System.out.println(lightPlayerName);

        //GameDTO gameDTO = gameService.startNewGame(lightPlayerName, darkPlayerName);
        
        gameService.startNewGame(lightPlayerName, darkPlayerName);

        //HttpSession session = req.getSession();
        //session.setAttribute("gameDTO", gameDTO);
        //req.getRequestDispatcher("WEB-INF/chessGame.jsp").forward(req, resp);
        resp.sendRedirect(req.getContextPath() + "/chessGameServlet");

    }

}
