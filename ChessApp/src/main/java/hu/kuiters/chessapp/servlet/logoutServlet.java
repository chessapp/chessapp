package hu.kuiters.chessapp.servlet;

import hu.kuiters.chessapp.dto.UserDTO;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Zsolt
 */
@WebServlet(name = "logoutServlet", urlPatterns = {"/logoutServlet"})
public class logoutServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        UserDTO user = (UserDTO) session.getAttribute("userDTO");

        if (user != null) {

            session.invalidate();
            req.getRequestDispatcher("WEB-INF/logout.jsp").forward(req, resp);
        } else {

            req.getRequestDispatcher("/index.jsp").forward(req, resp);
        }
    }

}
