package hu.kuiters.chessapp.util;

import hu.kuiters.chessapp.dto.GamePieceDTO;
import java.util.List;

public class ArrayListTo2DArrayConverter {

    public static final int ASCII_VALUE_MODIFICATOR = 97;
    public static final int ROW_MODIFICATOR_NUMBER = 8;

    public static String[][] placeUnicodesOfChessFigureIn2DArray(List<GamePieceDTO> pieceDTOs) {

        String[][] chessTable2DArray = new String[8][8];

        for (GamePieceDTO gamePiece : pieceDTOs) {

            int secondArrayIndex = gamePiece.getPosX() - ASCII_VALUE_MODIFICATOR;
            int firstArrayIndex = Math.abs(gamePiece.getPosY() - ROW_MODIFICATOR_NUMBER);
            chessTable2DArray[firstArrayIndex][secondArrayIndex] = gamePiece.getPiece().getUnicode();
           
        }

        return chessTable2DArray;
    }

}
