package hu.kuiters.chessapp.util;

import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.Session;

public class EmailSender {

    private static final String APP_EMAIL = "kuiters.java@gmail.com";
    private static final String PASSWORD = "kuiters2020";
    //public static final String TO_EMAIL = "tamas.scher@gmail.com"; //ezt csak a játék majd adatbázisból kell

    public static void sendEmail(String toEmail, String subject, String body) {

        Session session = sessionSetUpForEmail();

        try {
            MimeMessage msg = new MimeMessage(session);
            //set message headers
            msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
            msg.addHeader("format", "flowed");
            msg.addHeader("Content-Transfer-Encoding", "8bit");

            msg.setFrom(new InternetAddress(APP_EMAIL, "LevSakk"));

            msg.setReplyTo(InternetAddress.parse(APP_EMAIL, false));

            msg.setSubject(subject, "UTF-8");

            msg.setText(body, "UTF-8");

            msg.setSentDate(new Date());

            msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail, false));
            System.out.println("Message is ready");
            Transport.send(msg);

            System.out.println("EMail Sent Successfully!!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Session sessionSetUpForEmail() {

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
        props.put("mail.smtp.port", "587"); //TLS Port
        props.put("mail.smtp.auth", "true"); //enable authentication
        props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS

        //create Authenticator object to pass in Session.getInstance argument
        Authenticator auth = new Authenticator() {
            //override the getPasswordAuthentication method
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(APP_EMAIL, PASSWORD);
            }
        };
        Session session = Session.getInstance(props, auth);
        return session;

    }
    
    public static String createEmailBody(char actualPosX, int actualPosY, char targetPosX, int targetPosY) {
        String emailBody = "Hello, az ellenf�l " + actualPosX + actualPosY + "-r�l " + targetPosX + targetPosY + "-re l�pett. Te j�ssz.";
        
        return emailBody;
    }

}
