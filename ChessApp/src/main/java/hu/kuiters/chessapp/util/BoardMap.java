package hu.kuiters.chessapp.util;

import hu.kuiters.chessapp.entity.GamePiecesEntity;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Zsolt
 */
public class BoardMap {

    public static final Map<String, Integer> boardMap = new HashMap<>();

    public BoardMap() {
        boardMap.put("a8", 0);
        boardMap.put("b8", 1);
        boardMap.put("c8", 2);
        boardMap.put("d8", 3);
        boardMap.put("e8", 4);
        boardMap.put("f8", 5);
        boardMap.put("g8", 6);
        boardMap.put("h8", 7);
        boardMap.put("a7", 8);
        boardMap.put("b7", 9);
        boardMap.put("c7", 10);
        boardMap.put("d7", 11);
        boardMap.put("e7", 12);
        boardMap.put("f7", 13);
        boardMap.put("g7", 14);
        boardMap.put("h7", 15);
        boardMap.put("a6", 16);
        boardMap.put("b6", 17);
        boardMap.put("c6", 18);
        boardMap.put("d6", 19);
        boardMap.put("e6", 20);
        boardMap.put("f6", 21);
        boardMap.put("g6", 22);
        boardMap.put("h6", 23);
        boardMap.put("a5", 24);
        boardMap.put("b5", 25);
        boardMap.put("c5", 26);
        boardMap.put("d5", 27);
        boardMap.put("e5", 28);
        boardMap.put("f5", 29);
        boardMap.put("g5", 30);
        boardMap.put("h5", 31);
        boardMap.put("a4", 32);
        boardMap.put("b4", 33);
        boardMap.put("c4", 34);
        boardMap.put("d4", 35);
        boardMap.put("e4", 36);
        boardMap.put("f4", 37);
        boardMap.put("g4", 38);
        boardMap.put("h4", 39);
        boardMap.put("a3", 40);
        boardMap.put("b3", 41);
        boardMap.put("c3", 42);
        boardMap.put("d3", 43);
        boardMap.put("e3", 44);
        boardMap.put("f3", 45);
        boardMap.put("g3", 46);
        boardMap.put("h3", 47);
        boardMap.put("a2", 48);
        boardMap.put("b2", 49);
        boardMap.put("c2", 50);
        boardMap.put("d2", 51);
        boardMap.put("e2", 52);
        boardMap.put("f2", 53);
        boardMap.put("g2", 54);
        boardMap.put("h2", 55);
        boardMap.put("a1", 56);
        boardMap.put("b1", 57);
        boardMap.put("c1", 58);
        boardMap.put("d1", 59);
        boardMap.put("e1", 60);
        boardMap.put("f1", 61);
        boardMap.put("g1", 62);
        boardMap.put("h1", 63);
    }

    public void fillUp(GamePiecesEntity entity) {
        String i = String.valueOf(entity.getPosX()) + entity.getPosY();
        int a = boardMap.get(i);

    }
}
