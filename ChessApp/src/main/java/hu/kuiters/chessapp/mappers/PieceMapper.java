package hu.kuiters.chessapp.mappers;

import hu.kuiters.chessapp.dto.PieceDTO;
import hu.kuiters.chessapp.entity.PieceEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PieceMapper {

    PieceMapper INSTANCE = Mappers.getMapper(PieceMapper.class);

    PieceDTO toDTO(PieceEntity pieceEntity);

    PieceEntity toEntity(PieceDTO pieceDTO);

}
