package hu.kuiters.chessapp.mappers;

import hu.kuiters.chessapp.dto.GameDTO;
import hu.kuiters.chessapp.entity.GameEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 *
 * @author Zsolt
 */
@Mapper
public interface GameMapper {

    GameMapper INSTANCE = Mappers.getMapper(GameMapper.class);

    GameDTO toDTO(GameEntity gameEntity);

    GameEntity toEntity(GameDTO gameDTO);

    List<GameDTO> toDTOList(List<GameEntity> gameEntity);
}
