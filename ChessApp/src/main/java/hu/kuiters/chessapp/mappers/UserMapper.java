package hu.kuiters.chessapp.mappers;

import hu.kuiters.chessapp.dto.UserDTO;
import hu.kuiters.chessapp.entity.UserEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 *
 * @author Zsolt
 */
@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserDTO toDTO(UserEntity userEntity);

    UserEntity toEntity(UserDTO userDTO);

    List<UserDTO> toDTOList(List<UserEntity> userEntities);
}
