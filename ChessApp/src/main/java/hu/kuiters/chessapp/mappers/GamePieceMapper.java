package hu.kuiters.chessapp.mappers;

import hu.kuiters.chessapp.dto.GamePieceDTO;
import hu.kuiters.chessapp.entity.GamePiecesEntity;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface GamePieceMapper {

    GamePieceMapper INSTANCE = Mappers.getMapper(GamePieceMapper.class);

    GamePieceDTO toDTO(GamePiecesEntity gmaePieceEntity);

    GamePiecesEntity toEntity(GamePieceDTO gamePieceDTO);

    List<GamePieceDTO> toDTOList(List<GamePiecesEntity> gamePiecesEntities);

    List<GamePiecesEntity> toEntityList(List<GamePieceDTO> gamePieceDTOs);

}
